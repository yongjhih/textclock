package com.stylingandroid.textclock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Calendar;

/**
 * Copyright 2013 Mark Allison
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TextClockAppWidget extends AppWidgetProvider
{
    private static final String TAG = "TextClockWidget";
    private static final Intent update =
            new Intent( TextClockService.ACTION_UPDATE );
    private static final int REQUEST_CODE = 1;
    private Context context = null;

    @Override
    public void onUpdate( Context context,
                          AppWidgetManager appWidgetManager,
                          int[] appWidgetIds )
    {
        Log.d( TAG, "onUpdate" );
        this.context = context;
        this.context.startService( update );
        scheduleTimer();
    }

    private void scheduleTimer()
    {
        Calendar date = Calendar.getInstance();
        date.set( Calendar.SECOND, 0 );
        date.set( Calendar.MILLISECOND, 0 );
        date.add( Calendar.MINUTE, 1 );
        AlarmManager am =
                (AlarmManager) context.getSystemService(
                        Context.ALARM_SERVICE );
        PendingIntent pi = PendingIntent.getService( context,
                REQUEST_CODE,
                update,
                PendingIntent.FLAG_NO_CREATE );
        if ( pi == null )
        {
            pi = PendingIntent.getService( context,
                    REQUEST_CODE,
                    update,
                    PendingIntent.FLAG_CANCEL_CURRENT );
            am.setRepeating( AlarmManager.RTC,
                    date.getTimeInMillis(),
                    60 * 1000,
                    pi );
            Log.d( TAG, "Alarm created" );
        }
    }

    @Override
    public void onDeleted( Context context,
                           int[] appWidgetIds )
    {
        Log.d( TAG, "onDeleted" );
        AppWidgetManager mgr = AppWidgetManager.getInstance( context );
        int[] remainingIds = mgr.getAppWidgetIds(
                new ComponentName( context, this.getClass() ) );
        if ( remainingIds == null || remainingIds.length <= 0 )
        {
            PendingIntent pi = PendingIntent.getService( context,
                    REQUEST_CODE,
                    update,
                    PendingIntent.FLAG_NO_CREATE );
            if ( pi != null )
            {
                AlarmManager am =
                        (AlarmManager) context.getSystemService(
                                Context.ALARM_SERVICE );
                am.cancel( pi );
                pi.cancel();
                Log.d( TAG, "Alarm cancelled" );
            }
        }
    }
}
