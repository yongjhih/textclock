# Text Clock

Text Clock is a simple clock utility for Android which is released under the Apache 2.0 license. It is the accompanying
code for a series of articles on the technical blog Styling Android the first of which can be found
[here](http://blog.stylingandroid.com/archives/1501). It can also be installed directly from [Google Play](https://play.google.com/store/apps/details?id=com.stylingandroid.textclock).